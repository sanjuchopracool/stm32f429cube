import qbs 1.0
import qbs.FileInfo
import qbs.Environment

Product {
    type: ["application", "hex", "bin", "size"]
    Depends { name: "cpp" }
    cpp.executableSuffix: ".out"
    cpp.executablePrefix: ""

    Rule {
        id: hex
        inputs: ["application"]
        prepare: {
            var args = ["-O", "ihex", input.filePath, output.filePath];
            var objcopyPath = input.cpp.objcopyPath
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to hex: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["hex"]
            filePath: FileInfo.baseName(input.filePath) + ".hex"
        }
    }

    Rule {
        id: bin
        inputs: ["application"]
        prepare: {
            var objcopyPath = input.cpp.objcopyPath
            var args = ["-O", "binary", input.filePath, output.filePath];
            var cmd = new Command(objcopyPath, args);
            cmd.description = "converting to bin: "+ FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;

        }
        Artifact {
            fileTags: ["bin"]
            filePath: FileInfo.baseName(input.filePath) + ".bin"
        }
    }

    Rule {
        id: size
        inputs: ["application"]
        alwaysRun: true
        prepare: {
            var sizePath = input.cpp.objcopyPath.slice(0, -7) + "size"
            var args = [input.filePath];
            var cmd = new Command(sizePath, args);
            cmd.description = "File size: " + FileInfo.fileName(input.filePath);
            cmd.highlight = "linker";
            return cmd;
        }
        Artifact {
            fileTags: ["size"]
            filePath: undefined
        }
    }

    FileTagger {
        patterns: "*.ld"
        fileTags: ["linkerscript"]
    }

    cpp.cLanguageVersion: "gnu11"
    cpp.cxxLanguageVersion: "c++11"
    cpp.positionIndependentCode: false
    cpp.optimization: "none"
    cpp.debugInformation: true
    cpp.enableExceptions: false
    cpp.enableRtti: false
    cpp.enableReproducibleBuilds: true
    cpp.treatSystemHeadersAsDependencies: true

    cpp.driverFlags:
        [
        "-mcpu=cortex-m4",
        "-mthumb",
        "-mfloat-abi=hard",
        "-mfpu=fpv4-sp-d16",
        "--specs=nano.specs",
        "-ffunction-sections",
        "-fdata-sections",
        "-Wall",
        "-fstack-usage",
    ]

    cpp.cxxFlags: [
        "-fno-threadsafe-statics",
        "-fno-use-cxa-atexit"
    ]

    cpp.linkerFlags: [
        "--gc-sections",
        "-u,Reset_Handler"
    ]


    cpp.defines: [
        "USE_HAL_DRIVER",
        "USE_FULL_LL_DRIVER",
        "DEBUG",
        "STM32F429xx"
    ]


    property  string cubePackagePath: Environment.getEnv("HOME") + "/STM32Cube/Repository/STM32Cube_FW_F4_V1.24.1/"
    property  string firmwareDriverPath: cubePackagePath + "Drivers/"
    property string  driverPath: firmwareDriverPath + "STM32F4xx_HAL_Driver/"
    property string  driverIncludePath: driverPath + "Inc/"
    property string  driverLegacyIncludePath: driverIncludePath + "Legacy/"
    property string  driverSourcesPath: driverPath + "Src/"

    property  string cmsisPath: firmwareDriverPath + "CMSIS/"
    property string  cmsisIncludePath: cmsisPath + "Include/"
    property string  cmsisDeviceIncludePath: cmsisPath + "Device/ST/STM32F4xx/Include"

    property string middlewaresPath: cubePackagePath + "Middlewares/"
    property string freeRtosPath : middlewaresPath + "Third_Party/FreeRTOS/Source/"
    property string freeRtosIncludePath : freeRtosPath + "include/"
    property string freeRtosPortablePath: freeRtosPath + "portable/GCC/ARM_CM4F"
    property string freeRtosCmsisIncludePath : freeRtosPath + "CMSIS_RTOS/"

    property string middlewareSTPath: middlewaresPath + "ST/"
    property string stUSBHostLibPath: middlewareSTPath + "STM32_USB_Host_Library/"
    property string usbHostCoreIncludePath: stUSBHostLibPath + "Core/Inc/"
    property string usbHostCDCIncludePath: stUSBHostLibPath + "Class/CDC/Inc/"
    property  string projectSourcesPath: "Src/"
    cpp.includePaths:
        [
        ".",
        "./Inc",
        driverIncludePath,
        driverLegacyIncludePath,
        cmsisIncludePath,
        cmsisDeviceIncludePath,
        freeRtosPortablePath,
        freeRtosIncludePath,
        freeRtosCmsisIncludePath,
        usbHostCoreIncludePath,
        usbHostCDCIncludePath
    ]

    files: {
        var projectFiles = [
                    "./STM32F429ZITX_FLASH.ld",
                    "./Startup/startup_stm32f429zitx.S",
                    "./DEBUGGING.txt"
                ];

        var headers = [
                    "includes.h",
                    "systemclock.h",
                ]

        var sources = [
                    "system_stm32f4xx.c",
                    "systemclock.cpp",
                    "freertos.c",
                    "main.cpp",
                ]
        var librarySources = [
                    "stm32f4xx_ll_gpio.c",
                    "stm32f4xx_ll_utils.c",
                ]

        var freeRtosSources = [
                    "CMSIS_RTOS/cmsis_os.c",
                    "portable/GCC/ARM_CM4F/port.c",
                    "portable/MemMang/heap_4.c",
                    "list.c",
                    "tasks.c",
                    "queue.c",
                    "timers.c",
                    "croutine.c",
                    "event_groups.c",
                    "stream_buffer.c",
                    "readme.txt",
                ]


        var usbHostSources = [
                    "Class/CDC/Src/usbh_cdc.c",
                    "Core/Src/usbh_core.c",
                    "Core/Src/usbh_ctlreq.c",
                    "Core/Src/usbh_ioreq.c",
                    "Core/Src/usbh_pipes.c",
                ]

        for(var i=0; i<sources.length; i++){
            projectFiles.push( projectSourcesPath + sources[i]);
        }

        for(var i=0; i<librarySources.length; i++){
            projectFiles.push( driverSourcesPath + librarySources[i]);
        }

        for(var i=0; i<freeRtosSources.length; i++){
            projectFiles.push( freeRtosPath + freeRtosSources[i]);
        }

//        for(var i=0; i<usbHostSources.length; i++){
//            projectFiles.push( stUSBHostLibPath + usbHostSources[i]);
//        }

        for(var i=0; i<headers.length; i++){
            projectFiles.push( "./Inc/" + headers[i]);
        }

        return projectFiles;
    }

    cpp.staticLibraries:
        [
        "gcc",
        "c_nano",
        "m",
        "nosys",
    ]
}
