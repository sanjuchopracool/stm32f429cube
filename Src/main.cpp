
#include "includes.h"
#include "cmsis_os.h"

#include <systemclock.h>

extern "C"
{
void MX_FREERTOS_Init(void);
}


#define LD3_Pin LL_GPIO_PIN_13
#define LD3_GPIO_Port GPIOG
#define LD4_Pin LL_GPIO_PIN_14
#define LD4_GPIO_Port GPIOG

void init_leds()
{
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOG);
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

    GPIO_InitStruct.Pin = LD3_Pin|LD4_Pin;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOG, &GPIO_InitStruct);
}
void toggleLed(void * data)
{
    UNUSED(data);
    while (1)
    {
        vTaskDelay(1000);
        LL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
    }
}

int main(void)
{
    SystemClock::config();
    init_leds();
    MX_FREERTOS_Init();

    xTaskCreate( toggleLed, "LED_TOGGLE", 1000, nullptr, 1, nullptr );
    /* Start scheduler */
    osKernelStart();
    while (1)
    {
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
